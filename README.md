# REE-MIND-TOO-DOO App

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.12.



## About the app
This simple To Do app is a basic app that stores or removes tasks into a local datasource.  The datasource is cleared upon page refresh.  Taks can be added by simply entering a task description into the input field that states 'What needs to be done?'.  Added tasks can be removed by simply clicking on the trash (delete) icon next to the tasks description.

## Build and run
To build the app, simply run the `ng build` command to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

To run the app in development mode, run the `ng serve` command. Navigate to `http://localhost:4200/`. If port `4200` is already taken, confirm to use a new port and the app will automatically create a new port.  The app will also automatically reload if you change any of the source files.

## App Dependancies
The app runs on Angular v.11 with Angular Material.  Bootstrap is used for scaffolding.