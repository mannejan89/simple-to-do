import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

interface ITask {
  task: string,
  done: boolean
}

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  // tasks: ITask[] = [
  //   { task: 'Do some work', done: false }
  // ];

  private _tasks$: BehaviorSubject<ITask[]> = new BehaviorSubject([{ task: 'Do some work', done: false }]);
  public tasks = this._tasks$.asObservable();

  get tasksValue() {
    return this._tasks$.getValue()
  }

  constructor() { }

  addTask(task: ITask){
    this.tasksValue.push(task);
    this._tasks$.next(this.tasksValue)
  }

  removeTaskFromList(index: number): void {
    this.tasksValue.splice(index, 1);
    this._tasks$.next(this.tasksValue);
  }

  completeTask(value: boolean, index: number): void {
    let task = this.tasksValue.find((_, i) => i === index);
    task.done = value;
    this.tasksValue[index] = task;

    this._tasks$.next(this.tasksValue)
  }

  totalIncompleteTasks(): number{
    return this.tasksValue.filter(task => !task.done).length;
  }

  totalCompleteTasks(): number {
    return this.tasksValue.filter(task => task.done).length;
  }

}
