import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { LayoutModule } from './layout/layout.module';
import { TodoManagerModule } from './todo/todo-manager.module'

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,

    LayoutModule,
    TodoManagerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
