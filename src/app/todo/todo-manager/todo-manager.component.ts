import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'todo-manager',
  templateUrl: './todo-manager.component.html',
  styleUrls: ['./todo-manager.component.scss']
})
export class TodoManagerComponent implements OnInit {

  todoListFormGroup: FormGroup;

  tasks = this._taskService.tasks;

  constructor(
    private _taskService: TaskService,
    private _formBuilder: FormBuilder
  ) {
    this.todoListFormGroup = this._formBuilder.group({
      task: ['', Validators.required],
      done: false
    });
  }

  ngOnInit(): void {
  }

  addTaskToList(): void {
    this._taskService.addTask(this.todoListFormGroup.value);
    this.todoListFormGroup.reset();
    this.todoListFormGroup.clearValidators();
  }

  removeTaskFromList(event: boolean, index: number): void {
    if(event) this._taskService.removeTaskFromList(index);
  }

  completeTask(event: boolean, index: number): void {
    this._taskService.completeTask(event, index);
  }

  totalCompleteTasks(): number{
    return this._taskService.totalCompleteTasks();
  }

  totalIncompleteTasks(): number{
    return this._taskService.totalIncompleteTasks();
  }

}
