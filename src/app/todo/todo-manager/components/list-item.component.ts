import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';

@Component({
  selector: 'todo-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {

  @Input() public task: string = '';
  @Input() public checked: boolean = false;
  @Input() public canRemove: boolean = true;

  @Output() public removed = new EventEmitter<boolean>();
  @Output() public completed = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  removedClick(): void {
    this.removed.emit(true);
  }

  completedClick(event: MatCheckboxChange){
    this.completed.emit(event.checked);
  }

}
